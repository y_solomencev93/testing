from selenium import webdriver
import time

browser = webdriver.Chrome()
browser.maximize_window()
browser.get('https://my.mmtr.ru/')

def smoke_test():
    textarea = browser.find_element_by_xpath('//*[@id="inputLogin"]')
    time.sleep(5)
    textarea.send_keys('login')

    textarea = browser.find_element_by_xpath('//*[@id="inputPassword"]')
    time.sleep(5)
    textarea.send_keys('password')

    sumbit = browser.find_element_by_css_selector("[type=submit]")
    sumbit.click()
    time.sleep(1)

    report = browser.find_element_by_xpath('//*[@id="reports-tab"]')
    report.click()
    time.sleep(1)

    profile = browser.find_element_by_xpath('//*[@id="profile-tab"]')
    profile.click()
    time.sleep(1)

    competencies = browser.find_element_by_xpath('//*[@id="competencies-tab"]')
    competencies.click()
    time.sleep(2)

    exams = browser.find_element_by_xpath('//*[@id="exams-tab"]')
    exams.click()
    time.sleep(2)

    certificates = browser.find_element_by_xpath('//*[@id="certificates-tab"]')
    certificates.click()
    time.sleep(2)

    coaching = browser.find_element_by_xpath('//*[@id="coaching-tab"]')
    coaching.click()
    time.sleep(2)

    messages = browser.find_element_by_xpath('//*[@id="messages-tab"]')
    messages.click()
    time.sleep(2)

    lectures = browser.find_element_by_xpath('//*[@id="lectures-tab"]')
    lectures.click()
    time.sleep(2)

    organization_structure = browser.find_element_by_xpath('//*[@id="organization-structure-tab"]')
    organization_structure.click()
    time.sleep(2)

    vacation = browser.find_element_by_xpath('//*[@id="vacation-applications-tab"]')
    vacation.click()
    time.sleep(2)


smoke_test()

browser.close()
browser.quit()

print('Test Complited')