from selenium import webdriver
import time
from selenium.webdriver.common.keys import Keys

browser = webdriver.Chrome()
browser.maximize_window()
browser.get('https://www.seleniumeasy.com/test/basic-first-form-demo.html')

def close_add():
    btn_close = browser.find_element_by_xpath('//*[@id="at-cv-lightbox-button-holder"]/a[2]')
    btn_close.click()


def single_input_field():
    textfield = browser.find_element_by_xpath('//*[@id="user-message"]')
    textfield.send_keys('Test data')

    press_show_message = browser.find_element_by_xpath('//*[@id="get-input"]/button')
    press_show_message.click()

def two_input_fields():
    textfield = browser.find_element_by_xpath('//*[@id="sum1"]')
    textfield.send_keys('10')

    textfield = browser.find_element_by_xpath('//*[@id="sum2"]')
    textfield.send_keys('20')

    press_get_total = browser.find_element_by_xpath('//*[@id="gettotal"]/button')
    press_get_total.click()


single_input_field()

time.sleep(1)

close_add()

time.sleep(2)
two_input_fields()

browser.get('https://www.seleniumeasy.com/test/basic-checkbox-demo.html')
def alone_check():
    check = browser.find_element_by_css_selector("[type=checkbox]")
    check.click()
def multiple_check():
    option_1 = browser.find_element_by_xpath('//*[@id="easycont"]/div/div[2]/div[2]/div[2]/div[1]/label/input')
    option_1.click()
    time.sleep(1)
    option_2 = browser.find_element_by_xpath('//*[@id="easycont"]/div/div[2]/div[2]/div[2]/div[2]/label/input')
    option_2.click()
    time.sleep(1)
    option_3 = browser.find_element_by_xpath('//*[@id="easycont"]/div/div[2]/div[2]/div[2]/div[3]/label/input')
    option_3.click()
    time.sleep(1)
    option_4 = browser.find_element_by_xpath('//*[@id="easycont"]/div/div[2]/div[2]/div[2]/div[4]/label/input')
    option_4.click()
    time.sleep(1)

def radio_button_demo():
    browser.get('https://www.seleniumeasy.com/test/basic-radiobutton-demo.html')
    male = browser.find_element_by_css_selector('[value = Male]')
    male.click()

    get_checked = browser.find_element_by_id('buttoncheck')
    get_checked.click()

    time.sleep(2)

    female = browser.find_element_by_css_selector('[value = Female]')
    female.click()

    time.sleep(2)

    get_checked = browser.find_element_by_id('buttoncheck')
    get_checked.click()

def group_radio_button():
    male = browser.find_element_by_xpath('//*[@id="easycont"]/div/div[2]/div[2]/div[2]/div[1]/label[1]/input')
    male.click()
    time.sleep(1)
    age = browser.find_element_by_xpath('//*[@id="easycont"]/div/div[2]/div[2]/div[2]/div[2]/label[1]/input')
    age.click()
    time.sleep(1)
    get_values = browser.find_element_by_xpath('//*[@id="easycont"]/div/div[2]/div[2]/div[2]/button')
    get_values.click()
    time.sleep(1)

    age = browser.find_element_by_xpath('//*[@id="easycont"]/div/div[2]/div[2]/div[2]/div[2]/label[2]/input')
    age.click()
    time.sleep(1)
    get_values = browser.find_element_by_xpath('//*[@id="easycont"]/div/div[2]/div[2]/div[2]/button')
    get_values.click()
    time.sleep(1)

    age = browser.find_element_by_xpath('//*[@id="easycont"]/div/div[2]/div[2]/div[2]/div[2]/label[3]/input')
    age.click()
    time.sleep(1)
    get_values = browser.find_element_by_xpath('//*[@id="easycont"]/div/div[2]/div[2]/div[2]/button')
    get_values.click()

    time.sleep(5)

    male = browser.find_element_by_xpath('//*[@id="easycont"]/div/div[2]/div[2]/div[2]/div[1]/label[2]/input')
    male.click()
    time.sleep(1)
    age = browser.find_element_by_xpath('//*[@id="easycont"]/div/div[2]/div[2]/div[2]/div[2]/label[1]/input')
    age.click()
    time.sleep(1)
    get_values = browser.find_element_by_xpath('//*[@id="easycont"]/div/div[2]/div[2]/div[2]/button')
    get_values.click()
    time.sleep(1)

    age = browser.find_element_by_xpath('//*[@id="easycont"]/div/div[2]/div[2]/div[2]/div[2]/label[2]/input')
    age.click()
    time.sleep(1)
    get_values = browser.find_element_by_xpath('//*[@id="easycont"]/div/div[2]/div[2]/div[2]/button')
    get_values.click()
    time.sleep(1)

    age = browser.find_element_by_xpath('//*[@id="easycont"]/div/div[2]/div[2]/div[2]/div[2]/label[3]/input')
    age.click()
    time.sleep(1)
    get_values = browser.find_element_by_xpath('//*[@id="easycont"]/div/div[2]/div[2]/div[2]/button')
    get_values.click()


def select_demo_list():
    browser.get('https://www.seleniumeasy.com/test/basic-select-dropdown-demo.html')
    list_d = browser.find_element_by_css_selector("#select-demo [value='Sunday']").click()
    time.sleep(1)

    list_d = browser.find_element_by_css_selector("#select-demo [value='Monday']").click()
    time.sleep(1)

    list_d = browser.find_element_by_css_selector("#select-demo [value='Tuesday']").click()
    time.sleep(1)

    list_d = browser.find_element_by_css_selector("#select-demo [value='Wednesday']").click()
    time.sleep(1)

    list_d = browser.find_element_by_css_selector("#select-demo [value='Thursday']").click()
    time.sleep(1)

    list_d = browser.find_element_by_css_selector("#select-demo [value='Friday']").click()
    time.sleep(1)

    list_d = browser.find_element_by_css_selector("#select-demo [value='Saturday']").click()
    time.sleep(1)

def multi_select_demo_list():
    list_d = browser.find_element_by_css_selector("#multi-select [value='Florida']").click()
    first_btn = browser.find_element_by_xpath('//*[@id="printMe"]').click()

def contacts():
    browser.get('https://www.seleniumeasy.com/test/input-form-demo.html')
    field = browser.find_element_by_xpath('//*[@id="contact_form"]/fieldset/div[1]/div/div/input')
    field.send_keys('Iuri')

    field = browser.find_element_by_xpath('//*[@id="contact_form"]/fieldset/div[2]/div/div/input')
    field.send_keys('Solomencev')

    field = browser.find_element_by_xpath('//*[@id="contact_form"]/fieldset/div[3]/div/div/input')
    field.send_keys('emiratezkos@gmail.com')

    field = browser.find_element_by_xpath('//*[@id="contact_form"]/fieldset/div[4]/div/div/input')
    field.send_keys('(875)545-1217')

    field = browser.find_element_by_xpath('//*[@id="contact_form"]/fieldset/div[5]/div/div/input')
    field.send_keys('Russia')

    field = browser.find_element_by_xpath('//*[@id="contact_form"]/fieldset/div[6]/div/div/input')
    field.send_keys('Kostroma city')

    field = browser.find_element_by_xpath('//*[@id="contact_form"]/fieldset/div[7]/div/div/select').click()
    field = browser.find_element_by_xpath('//*[@id="contact_form"]/fieldset/div[7]/div/div/select/option[2]').click()

    field = browser.find_element_by_xpath('//*[@id="contact_form"]/fieldset/div[8]/div/div/input')
    field.send_keys('156025')

    field = browser.find_element_by_xpath('//*[@id="contact_form"]/fieldset/div[9]/div/div/input')
    field.send_keys('FaceBook')

    field = browser.find_element_by_xpath('//*[@id="contact_form"]/fieldset/div[10]/div/div[1]/label/input').click()

    field = browser.find_element_by_xpath('//*[@id="contact_form"]/fieldset/div[11]/div/div/textarea')
    field.send_keys('A lot of repeating text! A lot of repeating text! A lot of repeating text!')
    time.sleep(2)

    field = browser.find_element_by_xpath('//*[@id="contact_form"]/fieldset/div[13]/div/button').click()


def loading_form():
    browser.get('https://www.seleniumeasy.com/test/ajax-form-submit-demo.html')
    name = browser.find_element_by_xpath('//*[@id="title"]')
    name.send_keys('Iurii')

    name = browser.find_element_by_xpath('//*[@id="description"]')
    name.send_keys('Comments some like "Wow! It is great!"')
    time.sleep(5)
    submit = browser.find_element_by_xpath('//*[@id="btn-submit"]').click()


alone_check()
time.sleep(1)
multiple_check()
time.sleep(2)
radio_button_demo()
time.sleep(2)
group_radio_button()
time.sleep(2)
select_demo_list()
time.sleep(2)
multi_select_demo_list()
time.sleep(2)
contacts()
time.sleep(2)
loading_form()
time.sleep(2)

browser.close()
browser.quit()

print('Tests Complited')