import time

from allure_commons.types import Severity
from selenium.webdriver.chrome.webdriver import WebDriver
import allure


@allure.title('Смоук тест портала по основым вкладкам')
@allure.severity(Severity.BLOCKER)
@allure.description('Проверка перехода по основым вкладка портала')
def test_main_blocks():
    driver = WebDriver(executable_path='C://bin//chromedriver.exe')
    driver.maximize_window()
    with allure.step('Переходим на портал k1.news'):
        driver.get('https://k1news.ru/')
        time.sleep(5)

    with allure.step('Переходим на вкладку Новости'):
        block_news = driver.find_element_by_xpath('//*[@id="main-menu"]/nav/a[2]').click()
        time.sleep(1)

    with allure.step('Переходим на вкладку Партнеры'):
        block_partners = driver.find_element_by_xpath('//*[@id="main-menu"]/nav/a[3]').click()
        time.sleep(1)

    with allure.step('Переходим на вкладку Видео'):
        block_video = driver.find_element_by_xpath('//*[@id="main-menu"]/nav/a[4]').click()
        time.sleep(1)

    with allure.step('Переходим на вкладку Транспорт'):
        block_transport = driver.find_element_by_xpath('//*[@id="main-menu"]/nav/a[5]').click()
        time.sleep(1)

    with allure.step('Закрываем браузер'):
        driver.close()

    driver.quit()

@allure.title('Проверка ссылок на социальные сети')
@allure.severity(Severity.CRITICAL)
@allure.description('Проверка ссылок на социальные сети ВК, Фейсбук, Одноклассники и Твиттер')
def test_links_on_social_media():
    driver = WebDriver(executable_path='C://bin//chromedriver.exe')
    driver.maximize_window()
    with allure.step('Переходим на портал k1.news'):
        driver.get('https://k1news.ru/')

    with allure.step('Проверка ссылки на Вконтакте'):
        link_vk = driver.find_element_by_xpath('//*[@id="joinNowMenu"]/ul/li[1]/a').get_attribute('href')
        assert link_vk == 'http://vk.com/k1news_ru'

    with allure.step('Проверка ссылки на Фейсбук'):
        link_fb = driver.find_element_by_xpath('//*[@id="joinNowMenu"]/ul/li[2]/a').get_attribute('href')
        assert link_fb == 'http://www.facebook.com/k1news.ru'

    with allure.step('Проверка ссылки на Твиттер'):
        link_tw = driver.find_element_by_xpath('//*[@id="joinNowMenu"]/ul/li[3]/a').get_attribute('href')
        assert link_tw == 'https://twitter.com/#!/k1news1'

    with allure.step('Проверка ссылки на Одноклассники'):
        link_ok_ru = driver.find_element_by_xpath('//*[@id="joinNowMenu"]/ul/li[4]/a').get_attribute('href')
        assert link_ok_ru == 'http://www.odnoklassniki.ru/k1news.ru'

    with allure.step('Закрываем браузер'):
        driver.close()

    driver.quit()